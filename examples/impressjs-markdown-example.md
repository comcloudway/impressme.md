[rel-x]: 0
[rel-y]: 900
# Markdown 
## to author Impress.js presentations

* This presentation was written entirely in Markdown
* Added by popular request
  * Easy way to make quick, simple yet aesthetic, presentations
  * Authoring without all the clutter of HTML

-----
# Markdown.js

* Provided by [Markdown.js](https://github.com/evilstreak/markdown-js) in [extras/](https://github.com/impress/impress.js/tree/master/extras)
* Jot down your bullet points in *Markdown* & have it automatically converted to HTML
* Note: The Markdown is converted into a presentation client side, in the browser. This is unlike
  existing tools like [Hovercraft](https://github.com/regebro/hovercraft) and 
  markdown-impress where you generate a new
  html file on the command line.
* This combines the ease of typing Markdown with the full power of impress.js HTML5+CSS3+JavaScript!

-----
# Styles

* You can use *italics* & **bold**
* ...and `code`


-----
# A blockquote &amp; image

![Mother Teresa holding a newborn baby](images/3476636111_c551295ca4_b.jpg)

> Spread love everywhere you go.
> Let no one ever come to you without leaving happier.

*-- Mother Teresa*

Image credit: [Peta de Aztlan](https://www.flickr.com/photos/peta-de-aztlan/3476636111/)@Flickr. CC-BY 2.0

-----
# Code

When also using [Highlight.js](https://highlightjs.org/) integration, code blocks in Markdown
are converted to HTML first, then colored by Highlight.js:

        // `init` API function that initializes (and runs) the presentation.
        var init = function () {
            if (initialized) { return; }
            execPreInitPlugins();
            
            // First we set up the viewport for mobile devices.
            // For some reason iPad goes nuts when it is not done properly.
            var meta = $("meta[name='viewport']") || document.createElement("meta");
            meta.content = "width=device-width, minimum-scale=1, maximum-scale=1, user-scalable=no";
            if (meta.parentNode !== document.head) {
                meta.name = 'viewport';
                document.head.appendChild(meta);
            }        

-----

# Slide separator

* 5 dashes are used to separate slides:

`-----`

* Attributes from `div.step` element are simply repeated.
  * Better use relative positioning, lest your slides will all be on top of each other.
* If you need to set some attributes, just use HTML and create a div element, then write 
  Markdown inside each div.

=====
[class]: title
[x]: 2000
[y]: 5000
# Title slide
## This slide has different CSS class than the previous ones

=====

[rotate]: -85
[x]: 2000
[y]: 2000
[z]: 1000
[scale]: 4
# Simplicity of Markdown & Full power of Impress.js
  
* This slide sets its own `rotate`, `x`, `y`, and even `z` coordinates
* Note that the Mother Teresa slide earlier was pimped with some custom CSS, all the while
  the content was written in simple Markdown.

=====
[id]: overview
[y]: 4000
[x]: 5000
[scale]: 10
[rotate]: 5
