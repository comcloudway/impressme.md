use structopt::StructOpt;
use std::{
    collections::HashMap,
    io::{Read, Write},
    fs::File,
    path::PathBuf
};
use regex::Regex;

/// Commandline options
#[derive(StructOpt)]
#[structopt(name = "impressmymd")]
struct Options {
    /// specifies input file
    #[structopt(short, long, name = "INPUT FILE", parse(from_os_str))]
    input: PathBuf,
    /// specifies output file
    #[structopt(short,long, name = "OUTPUT FILE", parse(from_os_str))]
    output: PathBuf,
    /// html template
    #[structopt(short,long, name = "HTML TEMPLATE", parse(from_os_str))]
    template: PathBuf,
    /// specifies author
    #[structopt(long)]
    author: Option<String>,
    /// specifies title
    #[structopt(long)]
    title: Option<String>,
    /// specifies description
    #[structopt(long)]
    description: Option<String>,
}

struct Slide {
    content: String,
    options: HashMap<String,String>
}
impl Slide {
    /// initalise an empty slide
    fn new() -> Slide {
        Slide {
            content: String::new(),
            options: HashMap::new()
        }
    }

    /// convert a slide to html
    fn to_html(&self) -> String {
        let mut props = String::new();

        if let None = self.options.get("class") {
            // default class
            props.push_str(&format!(" class=\"slide step markdown\""));
        }

        for (key, val) in self.options.iter() {
            let mut k = String::from(key);
            if &k != "id" && &k != "class" {
                k = String::from("data-");
                k.push_str(key);
            }
            let mut v = String::from(val);
            if &k == "class" {
                v.push_str(" markdown slide step");
            }
            props.push_str(&format!(" {}=\"{}\"", k, v));
        }
        format!("<div{}>\n{}</div>", props, self.content)
    }
}

struct Presentation {
    // meta
    /// Title
    title: Option<String>,
    /// Author
    author: Option<String>,
    /// Description
    description: Option<String>,
    /// all the slides
    slides: HashMap<usize, Slide>,
}
impl Presentation {
    /// initalise an empty presentation
    fn new() -> Presentation {
        Presentation {
            slides: HashMap::new(),
            title: None,
            author: None,
            description: None
        }
    }

    /// import presentation from markdown
    fn load(&mut self, md: String) {
        let mut current:usize = 0;
        self.slides.insert(current, Slide::new());
        for line in md.lines() {
            // regex to match comments
            let ex = Regex::new(r"(\[)((\S)+)(\]: )((\S)+)").unwrap();

            // process line
            if line == "-----" {
                // seperates slide
                current+=1;
                self.slides.insert(current, Slide::new());
            } else if let Some(cap) = ex.captures(line) {
                // set option
                if let Some(key) = cap.get(2) {
                    // has a key
                    if let Some(val) = cap.get(5) {
                        // provides a value
                        if let Some(slide) = self.slides.get_mut(&current) {
                            slide.options.insert(key.as_str().to_string(), val.as_str().to_string());
                        }
                    }
                }
            } else {
                if let Some(slide) = self.slides.get_mut(&current) {
                    slide.content.push_str(line);
                    slide.content.push_str("\n");
                }
            }
        }
    }

    /// convert presentation to html
    fn to_html(&self, template: String) -> String {
        let mut builder = template.to_string();

        if let Some(author) = &self.author {
            builder = builder.replace("{{ AUTHOR }}", &author);
        }
        if let Some(title) = &self.title {
            builder = builder.replace("{{ TITLE }}", &title);
        }
        if let Some(desc) = &self.description {
            builder = builder.replace("{{ DESCRIPTION }}", &desc);
        }

        let mut slides = String::new();
        let mut current:usize = 0;
        while let Some(slide) = self.slides.get(&current) {
            slides.push_str(&slide.to_html());
            slides.push_str("\n\n");
            current+=1;
        }
        if slides.len() > 0 {
            builder = builder.replace("{{ SLIDES }}", &slides);
        }

        builder
    }
}

fn main() {
    let options = Options::from_args();

    let mut show = Presentation {
        slides: HashMap::new(),
        author: options.author,
        title: options.title,
        description: options.description
    };

    if let Ok(mut file) = File::open(options.input) {
        let mut markdown = String::new();
        match file.read_to_string(&mut markdown) {
            Ok(_) => {
                if let Ok(mut file) = File::open(options.template) {
                    let mut template = String::new();
                    match file.read_to_string(&mut template) {
                        Ok(_) => {
                            show.load(markdown);
                            let html = show.to_html(template);
                            if let Ok(mut of) = File::create(options.output) {
                                match of.write_all(html.as_bytes()) {
                                    Ok(_) => {
                                        println!("Successfully generated presentation");
                                    }, 
                                    Err(_) => {
                                        println!("Failed to write output file");
                                    }
                                }
                            } else {
                                println!("Failed to create output file");
                            }
                        },
                        Err(_) => {
                            println!("Failed to read html template");
                        }
                    }
                } else {
                    println!("Failed to open html template");
                }
            },
            Err(_) => {
                println!("Failed to read input file");
            }
        }
    } else {
        println!("Failed to open input file");
    }
}
